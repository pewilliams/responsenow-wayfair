library(data.table)
library(reshape2)
library(ggplot2)
library(lme4)
library(arm)

cdat <- readRDS('data/wayfairCPVandAA.rds')
#add weekend dummy
cdat <- transform(cdat, weekend = ifelse(Day %in% c('Sat','Sun'), 1,0), hour2 = Hour^2, logaa = log(aa), logaa2 = log(aa)^2, logp2 = log(p2))

#eda
ggplot(cdat, aes(x=log(aa), y=cpv)) + geom_point() + facet_wrap(~net) + geom_point(aes(x=log(p2), y = cpv, color='red'))

#definitely a different nested effect by network for aa
mod1 <- lmer(log(cpv)~ (logaa | net), data = cdat)
mod2 <- lmer(log(cpv)~ ( weekend  + logaa + Hour | net) , data = cdat)

lm1 <- lm(log(cpv)~ logaa + net, data = cdat)
lm2 <- lm(log(cpv)~ logp2 + net, data = cdat)

#what do the forecasts look like? 

cdat <- transform(cdat, cpvf = exp(fitted.values(mod2)), res = residuals(mod2))

#network
ggplot(cdat, aes(y=res, x=net))  + geom_boxplot()+ geom_jitter()

#hour
ggplot(cdat, aes(y=res, x=Hour))  + geom_point() + facet_wrap(~net) #some TBS and TNT concerns here


#weekend
ggplot(cdat, aes(y=res, x=weekend))  + geom_jitter() + facet_wrap(~net)
