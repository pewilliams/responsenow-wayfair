library(data.table)
library(reshape2)
library(ggplot2)
dyn.load(Sys.getenv('JVM_DYLIB'))
library(RJDBC)

hourCPV <- read.csv('data/wayfairQ316CPV.csv',header=T,stringsAsFactors=F)
daypartCPV <- read.csv('data/wayfairQ316Spots.csv',header=T,stringsAsFactors=F)
daypartCPV <- data.table(daypartCPV)
daypartCPV <- transform(daypartCPV, cpv = round(cpv, digits=2))
hourCPV <- data.table(hourCPV)

#clean up hour data
hourCPV <- melt.data.table(hourCPV, id.var=c('Hour','Day'))
hourCPV <- hourCPV[!is.na(value)]
setnames(hourCPV, c('variable','value'),c('net','cpv'))
hourCPV <- transform(hourCPV, Day = substr(Day, 1,3))

#change up the scale on the hours - 6a = 1, 5a = 29
hourCPV <- transform(hourCPV, Hour = ifelse(Hour > 5, Hour - 5, Hour + 19)) 


#ggplot(hourCPV, aes(x=Hour,y=log(cpv), color=net)) + geom_point() + facet_wrap(~Day) + geom_smooth(method='loess',se=F)

#grab audience data - 
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", Sys.getenv('JDBC_JAR'), "'")
conn <- dbConnect(drv, Sys.getenv('JDBC_AUDDATA'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD'))
tquery <- "select * from NPower where AudienceSegmentationCode = '2017 Q1 Wayfair LLC' and Universe > 0"
res <- dbSendQuery(conn, tquery) #send query 
tdat <- fetch(res, n = -1)
dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection
tdat <- data.table(tdat)
tdat <- transform(tdat, iDate = as.IDate(Date, '%Y-%m-%d'), hour = ceiling(HalfHourId/2))
tdat <- transform(tdat, Day = format(iDate, '%a'))

#subset to most recent quarter
tdat <- tdat[iDate > as.IDate('2016-03-25')] #max data is Jun 26 16
tdat <- tdat[,list(aa = mean(AAProj)),by=list(Network,hour,Day)]

#put data together
dcpv <- merge(hourCPV, tdat, by.x=c('Hour','Day','net'), by.y = c('hour','Day','Network'), all.x=T)

#dcpv <- readRDS('data/wayfairCPVandAA.rds')

adat <- readRDS('data/AudienceHistoryDatasets.rds')
adat <- adat[['History']]
adat <- transform(adat, iDate = as.IDate(Date), Hour = ceiling(HalfHourId/2), 
        Day = format(Date, '%a'))

adat <- adat[iDate >= as.IDate('2016-03-25') & iDate <= as.IDate('2016-06-28')]
adat <- adat[,list(p2 = mean(TargetP299)), by = list(NielsenNetwork,Hour,Day)]

#eda
dcpv <- merge(dcpv, adat, by.x=c('net','Day','Hour'), by.y=c('NielsenNetwork','Day','Hour') , all.x=T)

save(dcpv, file = 'data/wayfairCPVandAA.rds')
